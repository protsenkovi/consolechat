name := "ConsoleChat"

version := "0.1"

scalaVersion := "2.13.3"

// https://mvnrepository.com/artifact/com.github.java-json-tools/json-schema-validator
libraryDependencies += "com.github.java-json-tools" % "json-schema-validator" % "2.2.14"
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.11.0"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
