# Задание

Написать систему мгновенного обмена сообщениями между несколькими пользователями.

# Docker
Репозиторий образа https://hub.docker.com/repository/docker/protsenkovi/consolechat

Построение образа:
```
docker build -t protsenkovi/consolechat:niov1 .
```

Запуск сервера:
```
 docker run --rm -it -p 10000:10000 protsenkovi/consolechat:niov1
```

# Документация

Между клиентом и сервером происходит обмен JSON сообщениями вида:
```
{"senderId":"AD60","receiverId":"C399","body":"hello"}
```
   - senderId - идентификатор отправителя,  
   - receiverId - идентификатор получателя,  
   - body - текстовое сообщение. 
   
Сервер от своего имени отправляет клиентам приветственное сообщение при подключении. Это позволяет узнать назначенный сервером идентификатор на старте.

# Тестовый сервер

Адрес: consolechat.germanywestcentral.cloudapp.azure.com  
Порт: 10000