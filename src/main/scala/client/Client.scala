package client

import java.io.{BufferedReader, InputStreamReader}
import java.net.{InetSocketAddress, SocketAddress}
import java.nio.channels.{SelectionKey, Selector, SocketChannel}

import ch.qos.logback.classic.Level
import com.typesafe.scalalogging.{LazyLogging, Logger}
import datatypes.Message
import org.slf4j.LoggerFactory
import ch.qos.logback.classic.Level
import com.sun.mail.util.SocketConnectException

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.jdk.CollectionConverters._

class Client(host:String, port:Int, recieveCallback: Message=>Unit) extends Runnable with LazyLogging {

  @volatile var isRunning = false
  var clientId = "default"
  private val selector = Selector.open()
  private var clientChannel: SocketChannel = _
  private  val inbox = mutable.ArrayDeque[Message]()

  def start(): Unit = {
    isRunning = true

    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        Thread.currentThread().setName("CLEANER")
        if (isRunning) {
          logger.info("Closing connection.")
          isRunning = false
          Thread.sleep(10)
          if (clientChannel != null) clientChannel.close()
        }
      }
    })

    val clientThread = new Thread(this, "CLIENT")
    clientThread.start()
  }

  def leaveMessage(receiverId:String, body: String): Selector = {
    val msg = Message(senderId = this.clientId, receiverId = receiverId, body)
    inbox.addOne(msg)
    clientChannel.register(selector, SelectionKey.OP_WRITE)
    selector.wakeup()
  }

  def run (): Unit = {
    clientChannel = SocketChannel.open()
    try {
      clientChannel.connect(new InetSocketAddress(host, port))
      logger.info(s"Connected to $host:$port")
    } catch {
      case e: SocketConnectException =>
        logger.info("Connection failed.")
        System.exit(0)
    }

    clientChannel.configureBlocking(false)
    clientChannel.register(selector, SelectionKey.OP_READ)
    while(isRunning) {
      selector.select()
      for (key <- selector.selectedKeys().asScala) {
        if (key.isValid && key.isReadable) {
          try {
            val msg = util.Util.recieve[Message](clientChannel)
            if (msg.nonEmpty) {
              clientId = msg.get.receiverId
              if (msg.get.senderId != "server")
                recieveCallback(msg.get)
            }
          } catch {
            case e @ (_: com.fasterxml.jackson.databind.exc.MismatchedInputException |
                      _: java.io.IOException )=>
              logger.info(s"Client disconnected.")
              System.exit(0)
            case e @ (_: java.nio.channels.NotYetConnectedException |
                      _: java.nio.channels.NonReadableChannelException |
                      _: com.fasterxml.jackson.core.JsonProcessingException) =>
              logger.error(e.getMessage)
          }
        }
        if (key.isValid && key.isWritable) {
          if (inbox.nonEmpty) {
            try {
              util.Util.send(inbox.removeHead(), clientChannel)
              clientChannel.register(selector, SelectionKey.OP_READ)
            } catch {
              case e @ (_: com.fasterxml.jackson.databind.exc.MismatchedInputException |
                        _: java.io.IOException) =>
                logger.info(s"Client disconnected.")
                System.exit(0)
              case e @ (_: java.nio.channels.NotYetConnectedException |
                        _: java.nio.channels.NonReadableChannelException |
                        _: com.fasterxml.jackson.core.JsonProcessingException) =>
                logger.error(e.getMessage)
            }
          }
        }
      }
    }
  }
}

// TODO Exception: sbt.TrapExitSecurityException thrown from the UncaughtExceptionHandler in thread "CLIENT"
object Client extends LazyLogging {
  LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).asInstanceOf[ch.qos.logback.classic.Logger].setLevel(Level.INFO)

  val defaultArgs = Map(
    "host" -> "0.0.0.0",
    "port" -> "10000"
  )

  def uiLoop(client: Client): Unit = {
    Thread.currentThread().setName("UI")
    val consoleInput = new BufferedReader(new InputStreamReader(System.in))

    while(client.isRunning) {
      val line = consoleInput.readLine()
      if (line != null) {
        val receiverId = scala.util.Try(line.split(' ').filter(w => w(0) == '@').head.replace("@", ""))
          .getOrElse(util.Util.BROADCAST_RECIEVER_ID)

        val body = line.replace(s"@$receiverId ", "")

        client.leaveMessage(receiverId, body)
      }
    }
  }

  def main(args:Array[String]): Unit = {
    val argsMap = args.toSeq.sliding(2,2).map{ case ArraySeq(k,v) => (k.replace("--",""), v) }.toMap
      .withDefault(defaultArgs)
    val host = argsMap("host")
    val port = argsMap("port").toInt

    val client = new Client(host, port, message => {
      println(s"${message.senderId}: ${message.body}")
    })
    client.start()

    uiLoop(client)
  }
}