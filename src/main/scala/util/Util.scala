package util

import java.nio.ByteBuffer
import java.nio.channels.SocketChannel
import java.nio.charset.Charset
import java.security.MessageDigest

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable
import scala.reflect.ClassTag

object Util extends LazyLogging {
  val BUFFER_SIZE = 1024
  val ID_LIMIT = 2
  val BROADCAST_RECIEVER_ID = "-1"

  private val objectMapper = new ObjectMapper() with ScalaObjectMapper
  objectMapper.registerModule(DefaultScalaModule)
  private val charset = Charset.forName( "utf-8" )
  private val decoder = charset.newDecoder

  def recieveString(socketChannel:SocketChannel) : String = {

    val stringBuilder = new mutable.StringBuilder()
    val buffer = ByteBuffer.allocateDirect(BUFFER_SIZE)
    while (true) {
      val readBytes = socketChannel.read(buffer)
      if ((readBytes == 0) || (readBytes == -1))
        return stringBuilder.mkString
      buffer.flip()
      val charBuffer = decoder.decode( buffer )
      stringBuilder.appendAll(charBuffer.array())
    }

    ""
  }

  def sendString(str: String, socketChannel:SocketChannel): Unit = {
    logger.debug(s"Send: $str")
    val buffer = ByteBuffer.wrap(str.getBytes())
    socketChannel.write(buffer)
  }

  def recieve[T](sc:SocketChannel)(implicit ct: ClassTag[T]): Option[T] = {
    val raw = recieveString(sc)
    if ("".equals(raw)) return None
    val v = objectMapper.readerFor(ct.runtimeClass).readValue[T](raw)
    logger.debug(s"Recieve: ${v.toString}")
    Some(v)
  }

  def send[T](x:T, sc:SocketChannel)(implicit ct: ClassTag[T]): Unit =
    sendString(objectMapper.writerFor(ct.runtimeClass).writeValueAsString(x), sc)

  def genId(s: Any): String = {
    val md5 = java.security.MessageDigest.getInstance("MD5").digest(s.toString.getBytes)
    md5.map("%02X" format _).take(ID_LIMIT).mkString
  }

}