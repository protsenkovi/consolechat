package datatypes

import com.fasterxml.jackson.annotation.JsonProperty

case class Message(
    @JsonProperty("senderId") senderId:String,
    @JsonProperty("receiverId") receiverId:String,
    @JsonProperty("body")  body:String
)