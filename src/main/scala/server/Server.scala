package server

import java.io.IOException
import java.net.{InetAddress, InetSocketAddress, ServerSocket, Socket, SocketAddress, SocketOption}
import java.nio.ByteBuffer
import java.nio.channels.{SelectionKey, Selector, ServerSocketChannel, SocketChannel}
import java.util.concurrent.{ConcurrentHashMap, Executor, Executors, TimeUnit}

import ch.qos.logback.classic.Level
import com.fasterxml.jackson.core.JsonProcessingException
import com.typesafe.scalalogging.LazyLogging
import datatypes.Message
import org.slf4j.LoggerFactory

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.jdk.CollectionConverters._

class Server(host:String, port:Int) extends Runnable with LazyLogging {
  private val clientChannels = new mutable.HashMap[String, SocketChannel]()
  private val clientInboxes = new mutable.HashMap[String, mutable.ArrayDeque[Message]]()
  private val selector = Selector.open()

  @volatile var isRunning = false

  def run(): Unit = {
    logger.info(s"Server is running on $host:$port.")
    val serverSocketChannel = ServerSocketChannel.open()
    serverSocketChannel.bind(new InetSocketAddress(host, port))
    serverSocketChannel.configureBlocking(false)
    serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT)

    while (isRunning) {
      selector.select()
      for (key <- selector.selectedKeys().asScala) {
        if (key.isValid && key.isAcceptable) {
          val clientChannel = serverSocketChannel.accept()
          if (clientChannel != null) {
            clientChannel.configureBlocking(false)
            clientChannel.register(selector, SelectionKey.OP_WRITE)
            val clientId = util.Util.genId(clientChannel)
            clientChannels.put(clientId, clientChannel)
            clientInboxes.put(clientId, new mutable.ArrayDeque[Message]())
            clientInboxes(clientId).addOne(Message(senderId = "server", receiverId = clientId, body = "INIT"))
            logger.info(s"Client $clientId connected.")
          }
        }
        if (key.isValid && key.isReadable) {
          val clientChannel = key.channel().asInstanceOf[SocketChannel]
          val clientId = util.Util.genId(clientChannel)
          try {
            val msg = util.Util.recieve[Message](clientChannel)
            if (msg.nonEmpty) routeMessage(msg.get)
          } catch {
            case e @ (_: com.fasterxml.jackson.databind.exc.MismatchedInputException |
                      _: java.io.IOException ) =>
              clientChannel.close()
              clientChannels.remove(clientId)
              clientInboxes.remove(clientId)
              logger.info(s"Client $clientId disconnected.")
            case e @ (_ : java.nio.channels.NotYetConnectedException |
                      _ : java.nio.channels.NonReadableChannelException |
                      _ : com.fasterxml.jackson.core.JsonProcessingException) =>
              logger.error(e.getMessage, e.getCause)
          }
        }
        if (key.isValid && key.isWritable) {
          val clientChannel = key.channel().asInstanceOf[SocketChannel]
          val clientId = util.Util.genId(clientChannel)
          val clientInbox = clientInboxes(clientId)
          if (clientInbox.nonEmpty)
            try {
              util.Util.send(clientInbox.removeHead(), clientChannel)
              clientChannel.register(selector, SelectionKey.OP_READ)
            } catch {
              case e @ (_: com.fasterxml.jackson.databind.exc.MismatchedInputException |
                        _: java.io.IOException ) =>
                clientChannel.close()
                clientChannels.remove(clientId)
                clientInboxes.remove(clientId)
                logger.info(s"Client $clientId disconnected.")
              case e @ (_ : java.nio.channels.NotYetConnectedException |
                        _ : java.nio.channels.NonReadableChannelException |
                        _ : com.fasterxml.jackson.core.JsonProcessingException) =>
                logger.error(e.getMessage, e.getCause)
            }
        }
      }
    }
  }

  private def routeMessage(msg: Message) = {
    msg.receiverId match {
      case recieverId if recieverId != util.Util.BROADCAST_RECIEVER_ID =>
        if (clientChannels.keySet.contains(msg.receiverId)) {
          val cid = msg.receiverId
          val clientChannel = clientChannels(cid)
          val clientInbox = clientInboxes(cid)
          clientInbox.addOne(msg)
          clientChannel.register(selector, SelectionKey.OP_WRITE)
        }
      case _ =>
        for ((cid, inbox) <- clientInboxes.iterator)
          if (cid != msg.senderId) {
            val directMsg = Message(senderId=msg.senderId, receiverId = cid, body = msg.body)
            inbox.addOne(directMsg)
            clientChannels(cid).register(selector, SelectionKey.OP_WRITE)
          }
    }
  }

  def start(): Unit = {
    isRunning = true
    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        if (isRunning) {
          logger.info("Closing connections.")
          isRunning = false
          Thread.sleep(10)
          for (ch <- clientChannels.values) {
            try ch.close()
          }
        }
      }
    })
    this.run()
  }
}

object Server {
  LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).asInstanceOf[ch.qos.logback.classic.Logger].setLevel(Level.DEBUG)

  val defaultArgs = Map(
    "host" -> "0.0.0.0",
    "port" -> "10000"
  )

  // what happen when client disconnects when selector is:
  //  - waiting
  //  - just selected keys
  // when key
  //  - checking isValid
  //  - checking isReadable or isWritable
  // when reading from channel
  // when writing to channel

  // how socket connection and key.isConnectable are related
  // how socket connection and socket.isConnected are related
  def main(args:Array[String]): Unit = {
    val argsMap = args.toSeq.sliding(2,2).map{ case ArraySeq(k,v) => (k.replace("--",""), v) }.toMap
      .withDefault(defaultArgs)
    val host = argsMap("host")
    val port = argsMap("port").toInt

    val server = new Server(host, port)
    server.start()
  }
}