FROM ubuntu:20.10

SHELL ["/bin/bash", "-c"]

RUN apt-get update
RUN apt-get install curl zip unzip -y
RUN curl -s "https://get.sdkman.io" | bash
RUN source "/root/.sdkman/bin/sdkman-init.sh"; sdk install java; sdk install sbt
ENV PATH "/root/.sdkman/candidates/sbt/current/bin:/root/.sdkman/candidates/java/current/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
COPY . "/consolechat/"
WORKDIR "/consolechat/"
RUN sbt compile

EXPOSE 10000/tcp
CMD sbt "runMain server.Server --host 0.0.0.0 --port 10000"